from django.db import models
from projects.models import Project
from django.contrib.auth.models import User


# Create your models here.
class Task(models.Model):
    name = models.CharField(max_length=200)
    initiation_date = models.DateTimeField()
    estimated_completion = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project,
        related_name="tasks",
        on_delete=models.CASCADE,
    )
    agent = models.ForeignKey(
        User,
        null=True,
        related_name="tasks",
        on_delete=models.CASCADE,
    )
