from django.urls import path
from tasks.views import create_tasks, my_task_list

urlpatterns = [
    path("create/", create_tasks, name="create_task"),
    path("mine/", my_task_list, name="show_my_tasks"),
]
