from django.urls import path
from projects.views import (
    show_projects_list,
    show_project_details,
    create_project_list,
)


urlpatterns = [
    path("", show_projects_list, name="list_projects"),
    path("<int:id>", show_project_details, name="show_project"),
    path("create/", create_project_list, name="create_project"),
]
